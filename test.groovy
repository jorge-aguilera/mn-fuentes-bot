
float metersTo(float lat1, float lng1, float lat2, float lng2) {
    double radioTierra = 6371;
    double dLat = Math.toRadians(lat2 - lat1);
    double dLng = Math.toRadians(lng2 - lng1);
    double sindLat = Math.sin(dLat / 2);
    double sindLng = Math.sin(dLng / 2);
    double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
    double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
    double meters = radioTierra * va2;
    meters as float;
}

float[]extract( String name ){
    def matcher = name =~ /Lat_([\d\.]+)_Log_([-\d\.]+)/
    [ matcher[0][1] as float, matcher[0][2] as float]
}

File root = new File('build/fuentes')

float latitud = 40.4049522
float longitud = -3.6319141

println root.listFiles().sort { f ->
    (lat, lng) = extract(f.name.split('\\.').dropRight(1).join('.'))
    metersTo(latitud, longitud, lat, lng)
}.take(2)*.name
