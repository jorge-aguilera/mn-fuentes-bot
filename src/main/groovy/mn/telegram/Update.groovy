package mn.telegram

import groovy.transform.ToString

@ToString
class Update {

    int update_id

    UserMessage message

    CallbackQuery callback_query

}
