package mn.telegram

import groovy.transform.ToString

@ToString(includeNames = true)
class Location {

    float longitude
    float latitude


    float metersTo( Location b) {
        metersTo(this.latitude, this.longitude, b.latitude, b.longitude)
    }

    float metersTo( float lat2, float lng2) {
        metersTo(this.latitude, this.longitude, lat2, lng2)
    }

    static float metersTo(float lat1, float lng1, float lat2, float lng2) {
        double radioTierra = 6371;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
        double meters = radioTierra * va2;
        meters as float;
    }

}
