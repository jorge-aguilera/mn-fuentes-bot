package mn.telegram
import groovy.transform.ToString

@ToString(includeNames = true)
class InlineKeyboardMarkup {

    List<List<InlineKeyboardButton>> inline_keyboard = []

}
