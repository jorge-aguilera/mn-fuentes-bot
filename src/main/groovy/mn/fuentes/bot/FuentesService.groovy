package mn.fuentes.bot

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import io.micronaut.context.annotation.Value
import mn.telegram.Location
import javax.inject.Singleton

@Singleton
class FuentesService {

    @Value('${fuentes.path}')
    String fuentesPath

    @CompileStatic(TypeCheckingMode.SKIP)
    float[]extract( String name ){
        def matcher = name =~ /Lat_([\d\.]+)_Log_([-\d\.]+)/
        [ matcher[0][1] as float, matcher[0][2] as float]
    }

    List<Fuente> buscaFuentes(Location location, int max=1){

        List<Fuente> ret = []

        List<File> files = new File(fuentesPath).listFiles() as List<File>

        files.sort { f ->
            float[]coords = extract(f.name.split('\\.').dropRight(1).join('.'))
            location.metersTo( coords[0], coords[1])
        }

        files = files.take(max)

        files.each{ File f ->
            float[]coords = extract(f.name.split('\\.').dropRight(1).join('.'))
            float meters = location.metersTo(coords[0], coords[1])
            ret.add new Fuente(
                    calle: f.text,
                    location: new Location(latitude: coords[0], longitude: coords[1]),
                    distancia: meters)
        }

        ret

    }

}
