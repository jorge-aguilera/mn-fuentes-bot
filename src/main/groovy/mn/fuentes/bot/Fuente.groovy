package mn.fuentes.bot

import groovy.transform.ToString
import mn.telegram.Location

@ToString(includeNames = true)
class Fuente {

    String calle
    Location location
    float distancia

    String toMarkdown(){
        String.format("""
Hay una fuente a **%3.0f m**
en __%s__""",
                distancia*1000,
                calle
        )
    }

    Fuente copyFrom(Fuente f){
        this.calle = f.calle
        this.location = f.location
        this
    }

}
