package mn.fuentes.bot

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.reactivex.Single
import mn.telegram.InlineKeyboardButton
import mn.telegram.InlineKeyboardMarkup
import mn.telegram.Location
import mn.telegram.Message
import mn.telegram.TelegramClient
import mn.telegram.Update

@Controller('/${telegram.token}/fuentes')
class FuentesController {

    FuentesService fuentesService

    TelegramClient telegramClient

    FuentesController (FuentesService fuentesService, TelegramClient telegramClient){
        this.fuentesService = fuentesService
        this.telegramClient = telegramClient
    }

    void hello(String chatId){

        Message msg = new Message(chat_id: chatId, text: """
Puedo indicarte donde se encuentra la fuente pública más cercana 
pero para ello tienes que mandarme tu posicion

Selecciona `enviar adjunto` (el clip) y pincha en Ubicación.
Una vez me mandes donde estas te enviaré un mapa con la fuente más cercana
y si lo abres obtendrás indicaciones para ir a ella


Made with 💙 by Puravida Software 
Usando los catálogos de datos abiertos del 
Ayto de Madrid 
Ayto de Barcelona 
Ayto de Caceres y otros
""")
        telegramClient.sendMessage(msg).subscribe()

    }

    void sendFuente(String chatId, Location location) {

        Fuente ret = fuentesService.buscaFuentes(location).first()

        List<List<InlineKeyboardButton>> buttons = [
                [
                        new InlineKeyboardButton(
                                text: "Localizar en mapa",
                                callback_data: "location $ret.location.latitude $ret.location.longitude"
                        )
                ],
                [
                        new InlineKeyboardButton(
                                text: "Buscar Otra",
                                callback_data: "next $ret.location.latitude $ret.location.longitude"
                        )
                ],
        ]

        Message msg = new Message(
                chat_id: chatId,
                text: ret.toMarkdown(),
                reply_markup: new InlineKeyboardMarkup(inline_keyboard: buttons)
        )

        telegramClient.sendMessage(msg).subscribe({}, {})
    }

    void sendLocation(String chatId, float latitude, float longitude){
        telegramClient.sendLocation( chatId, latitude, longitude)
                .subscribe({},{})
    }

    void sendNext(String chatId, float latitude, float longitude){
        Location location = new Location(latitude: latitude, longitude: longitude)
        Fuente ret = fuentesService.buscaFuentes(location, 2).last()

        List<List<InlineKeyboardButton>> buttons = [
                [
                        new InlineKeyboardButton(
                                text: "Localizar en mapa",
                                callback_data: "location $ret.location.latitude $ret.location.longitude"
                        )
                ]
        ]

        Message msg = new Message(
                chat_id: chatId,
                text: ret.toMarkdown(),
                reply_markup: new InlineKeyboardMarkup(inline_keyboard: buttons)
        )

        telegramClient.sendMessage(msg).subscribe({}, {})
    }

    @Get('/{lat}/{log}')
    Single<Fuente>test(float lat, float log){
        Single.create({ emitter ->
            Location location = new Location(latitude: lat, longitude: log)
            Fuente ret = fuentesService.buscaFuentes(location).first()
            emitter.onSuccess(ret)
        })
    }

    @Post('/')
    String index(Update update){
        Single.create({ emitter ->

            if( update.message ) {

                if (update.message.location == null) {
                    hello(update.message.chat.id)
                }else{
                    sendFuente(update.message.chat.id, update.message.location)
                }
            }

            if( update.callback_query ){
                String chatId = update.callback_query.message.chat.id
                String[]args = update.callback_query.data.split(' ')

                switch( args.first() ){
                    case 'location':
                        sendLocation(chatId, args[1] as float, args[2] as float)
                        break
                    case 'next':
                        sendNext(chatId, args[1] as float, args[2] as float)
                        break
                }
            }

            emitter.onSuccess("done")

        }).subscribe()

        "done"
    }


}
