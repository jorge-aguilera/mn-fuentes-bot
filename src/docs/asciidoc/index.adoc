= Fuentes bot
jorge.aguilera@puravida-software.com


FuentesBot is a Telegram bot to find the public fontaint close to your position (Madrid or Barcelona, Spain).


== Project

You can find the project at https://gitlab.com/jorge-aguilera/mn-fuentes-bot

== Architecture

[plantuml]
----
@startuml
!define SPRITESURL https://raw.githubusercontent.com/rabelenda/cicon-plantuml-sprites/v1.0/sprites
!define PVIDAURL https://raw.githubusercontent.com/jagedn/mn-plantuml-sprites/master/sprites/

!includeurl SPRITESURL/java.puml
!includeurl PVIDAURL/micronaut.puml
!includeurl PVIDAURL/groovy.puml


title Fuentes TelegramBot


actor User as user
rectangle "Telegram" as telegram
rectangle "OpenData\nMadrid" as madrid
rectangle "OpenData\nBarcelona" as barcelona
rectangle "OpenData\nGranada" as granada

node "<$micronaut>\nBot" as bot{

    [Controller]
    [Controller] - [TelegramApi]
    [Service]
    folder "Files"{
        [fuentes]
    }
}

node "Build" as build{
   [Gradle]
}

user <---> telegram
telegram --> Controller
TelegramApi --> telegram
Controller -> Service
Service -> fuentes

Gradle .. madrid : CSV
Gradle .. barcelona : CSV
Gradle .. granada : CSV
Gradle -> fuentes
@enduml
----

== TelegramApi

[source,groovy]
----
include::{sourcedir}/mn/telegram/TelegramClient.groovy[]
----

== Controller

[source,groovy]
----
include::{sourcedir}/mn/fuentes/bot/FuentesController.groovy[]
----


== BotFather

Before to deploy a bot you need to create it and obtain a token via `BotFather`, a Telegram bot who create bots.
Following Telegram instructions you'll have a token similar to 'AAABBB:1234433221'

We'll use this token to:

- customize the controller url to avoid scrapper to find it
- send messages to the user (via POST)

It is a bad idea to versionate this token so we'll write it in our `gradle.properties` and sustitute the `telegram.token`
environment variable at runtime

== Heroku

To deploy the project in Heroku you need an account and create an app. Also you'll need to have `heroku cli` installed
into your machine.

The easy way to deploy an application to Heroku is to build a Docker image and deploy to it so we'll have some
gradle tasks to do this:

- buildDocker
- pushDocker
- releaseDocker

(As this project can serve public fountains for Madrid and Barcelona we'll have same tasks for Barcelona changing the
telegram token)

